cargo build --release --target wasm32-unknown-unknown && \

for file in ./target/wasm32-unknown-unknown/release/*; do
  if [[ $file == *.wasm ]]
  then
    wasm_file="$file"
  fi
done && \

wasm-bindgen $wasm_file --out-dir wasm --no-modules --no-typescript && \
echo "JS bindings created" && \
cd wasm && \
for file in ./*; do
  if [[ ("$file" == *.js) && !($"file" == blob.js) ]]
  then
    mv "$file" blob.js > /dev/null 2>&1
  elif [[ "$file" == *bg.wasm ]]
  then
    mv "$file" blob.wasm > /dev/null 2>&1
  fi
done && \
echo "File names updated and ready to deploy"
