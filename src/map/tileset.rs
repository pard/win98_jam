#[derive(Clone, Copy, PartialEq)]
pub enum TileType {
    Floor,
    Wall,
}
