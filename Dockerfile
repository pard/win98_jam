FROM nginx:stable-alpine
WORKDIR /etc/nginx/
RUN sed -i '$ d' mime.types && echo "application/wasm wasm;}" >> mime.types
WORKDIR /
COPY wasm /usr/share/nginx/html
EXPOSE 80
